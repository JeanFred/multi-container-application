<?php
header('Content-Type: application/json');

session_start();

if (!isset($_SESSION['count'])) {
    $_SESSION['count'] = 0;
}

$_SESSION['count'] = $_SESSION['count'] + 1;

echo '{';
echo '"date": ';
echo '"' . date("Y-m-d") .'",';
echo '"views": ';
echo '"' . $_SESSION['count'] .'",';
echo '"ip_address": ';
echo '"' . $_SERVER["REMOTE_ADDR"] . '"';
echo '}';
