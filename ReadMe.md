Multi-container Docker application
==================================

```
docker-compose up -d
docker-compose scale web=2
```

* Web-app: http://localhost
* HAproxy stats: http://localhost:1936/
* Kibana: http://localhost:5601/
* Jenkins: http://localhost:8081/

P.S. ElasticSearch may complain - had to run `sudo sysctl -w vm.max_map_count=262144`
